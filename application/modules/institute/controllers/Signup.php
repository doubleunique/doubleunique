<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['company_email'] = array(
				'name'  => 'company_email',
				'id'    => 'company_email',
				'type'  => 'text',
				'size'		=>	32,
				'maxlength'	=>	128,				
				'value' => $this->form_validation->set_value('company_email'),
				);
		$data['country'] = array(
				'name'  => 'country',
				'id'    => 'country',
				'type'  => 'text',
				'size'		=>	32,
				'maxlength'	=>	128,				
				'value' => $this->form_validation->set_value('country'),
				);
		$this->load->view('Signup',$data);
	}
}
 
