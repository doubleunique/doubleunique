<?php
class Common_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function insert($table,$data)
    {
		$this->db->_protect_identifiers=true;
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }
    
    function insert_batch($table,$data)
    {	
		$this->db->_protect_identifiers=true;
        $this->db->insert_batch($table,$data);
		//return $this->db->insert_id();
    }
    
    function update($table,$where=array(),$data)
    {
        $this->db->update($table,$data,$where);
        return $this->db->affected_rows();
    }
    
    function updateIncrement($table,$where=array(),$data)
    {
        foreach ($data AS $k => $v)
		{
			$this->db->set($k, $v, FALSE);
		}
        foreach ($where AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->update($table);
		return $this->db->affected_rows();
    }
    
    function update_batch($table,$where,$data)
    {
		$this->db->_protect_identifiers=true;
        $this->db->update_batch($table,$data,$where);
       // echo $this->db->last_query();
        return $this->db->affected_rows();
    }
    function delete($table,$where=array())
    {
        $this->db->delete($table,$where);
        return $this->db->affected_rows();
    }
    
    function delete_batch($table,$key,$ids=array())
    {	
		$this->db->where_in($key,$ids);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
    
    function select($sel,$table,$cond = array())
	{
		$this->db->select($sel, FALSE);
		$this->db->from($table);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function selectQuery($sel,$table,$cond = array(),$orderBy=array())
	{
		$this->db->select($sel, FALSE);
		$this->db->from($table);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
    
}
?>
